
$('.signin-button').click((event) => {
    event.preventDefault();
    login();
    
})

function login() {
    $.ajax({
        url: '../php/login.php',
        type: 'POST',
        dataType: 'json',
        data: {
            email: $('.user-input-email').val(),
            pwd: $('.pass-input').val(),
        },
        success: (res) => {
            console.log(res)
            if (res.success) {
                window.alert('Login réussi')
                window.location.replace('home.html')
            }
        }
    })
}

