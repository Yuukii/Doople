$('.logout').click((event) =>{
    event.preventDefault()
})
console.log(localStorage)
function logout() {
    $.ajax({
        url: '../php/home.php',
        type: 'GET',
        dataType: 'json',
        data: {
            choice: 'logout',
        },
        success: (res) => {
            console.log(res);
            console.log(localStorage)
            if (res.success) {
                localStorage.removeItem();
                window.location.replace('login.html')
            }
        }
    })
}
