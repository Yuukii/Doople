

$.ajax({
    url: '../php/admin.php',
    type: 'GET',
    dataType: 'json',
    data: {
        choice: 'select'
    },
    success: (res) => {
        console.log(res);
        if (res.success){
            console.log(res.clients);
            res.clients.forEach(client => {
                
                const tr = $('<tr></tr>');
                
                const id_client = $('<td></td>').text(client.ID_client);
                const username = $('<td></td>').text(client.username);
                const prenom = $('<td></td>').text(client.prenom);
                const nom = $('<td></td>').text(client.nom);
                const email = $('<td></td>').text(client.email);
                const telephone = $('<td></td>').text(client.telephone);
                const age = $('<td></td>').text(client.age);
                const ville = $('<td></td>').text(client.ville);
                const code_postal = $('<td></td>').text(client.code_postal);
                const numero_de_rue = $('<td></td>').text(client.numero_de_rue);
                const nom_de_la_rue = $('<td></td>').text(client.nom_de_la_rue);
                
                
                const update = $('<button></button>').text('Modifier');
                const tdupdate = $('<td></td>').append(update);
                update.attr('id', 'update_' + client.ID_client);



                
                const del = $('<button></button>').text('Supprimer');
                const tddel = $('<td></td>').append(del);
                del.attr('id', 'del_' + client.ID_client)
                
                tr.append(id_client, username, prenom, nom, email, telephone, age, ville, code_postal, numero_de_rue, nom_de_la_rue, tdupdate, tddel);
                $('table').append(tr);
                
                $('#update_' + client.ID_client).click (()=> {
    
                    if (confirm("etes vous sur de vouloir modifier cet article")) {

                        
                           $('#modal').css('display', 'block');
                           $('#modal-text').val(client.username);
                           $('#modal_update').click(() => {
                                updateUser(client.ID_client);
                           })
                    }
                });
                
                $('#del_' + client.ID_client).click (() => {
                    if (confirm("etes vous sur de vouloir supprimer cet article")) {
                        deleteUser(client.ID_client)
                    }
                });
            });
        }
    }
})


$('.close').click(() => {

    $('#modal').css('display', 'none');

})


function deleteUser(ID_client) {

    $.ajax({
        url: '../php/admin.php',
        type: 'GET',
        dataType: 'json',
        data: {
            choice: 'delete',
            id: ID_client
        },
        success: (res) => {

            $('#del_' + ID_client).remove();
                console.log(res)
                console.log(ID_client)
        }

    });
}

function updateUser(ID_client) {

    $.ajax({
        url: '../php/admin.php',
        type: 'GET',
        dataType: 'json',
        data: {
            choice: 'update',
            id: ID_client,
            user: username
            
        },
        success: (res) => {
            if (res.success){

                $('#modal_update' + ID_client ).append(username);
                console.log(res)

            }
        }
    })
    
}
