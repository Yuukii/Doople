
$('.register-button').click((event) => {

    event.preventDefault();
    inscription();
})


function inscription() {
     $.ajax({
         url: '../php/register.php',
         type: 'POST',
         dataType: 'json',
         data: {
             username: $('.user-input-name').val(),
             pwd: $('.pass-input').val(),
             prenom: $('.firstname').val(),
             nom: $('.lastname').val(),
             email: $('.user-input-email').val(),
             telephone: $('.phone').val(),
             age: $('.date').val(),
             ville: $('.city').val(),
             code_postal: $('.code_postal').val(),
             numero_de_rue: $('.numero_rue').val(),
             nom_de_la_rue: $('.rue').val()
         },
         success: (res) => {
             console.log(res);
             if (res.success) {
                 window.alert("inscription realisé");
                 window.location.replace('login.html');
             }
         }
     });
}
