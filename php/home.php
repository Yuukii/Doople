<?php
session_start();

if ($_SESSION['admin']){

    echo json_encode(["success" => true, "admin" => "oui"]);
}

switch ($method['choice']) {

    case 'logout':
        if ($_SESSION()) {
            session_destroy();
            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false, 'error' => 'session inexistante']);
        }
        break;
}
?>