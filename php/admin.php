<?php
session_start();
error_reporting(-1);

require_once("../php/db_connect.php");

if (!$_SESSION['connected']) {
    echo json_encode(["success" => false, "error" => "Vous n'êtes pas connecté"]);
    die;
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') $method = $_POST;
else $method = $_GET;

switch ($method['choice']) {

    case 'select':
        $clientpdo = $db->query("SELECT ID_client, username, prenom, nom, email, telephone, age, ville, code_postal, numero_de_rue, nom_de_la_rue  FROM Client");

        while ($client = $clientpdo->fetch(PDO::FETCH_ASSOC)) $clients[] = $client;

        echo json_encode(["success" => true, "clients" => $clients]);
        break;

    case 'update':
        if (
            isset($method['username'], $method['id']) &&
            !empty(trim($method['username']))&&
            !empty(trim($method['id']))
        ) {
            $sql = "UPDATE Client SET username = :username WHERE ID_client = :ID_client";
            $req = $db->prepare($sql);
            $req->bindValue(':username', $method['username']);
            $req->bindValue(':ID_client', $method['ID_client']);
            $req->execute();

            echo json_encode(["success" => true]);
        } else echo json_encode(["success" => false, "error" => "Données incorrectes"]);
        break;

    case 'delete':
        if (
            isset($method['id']) && 
            !empty(trim($method['id']))
            ) {
            $req = $db->prepare("DELETE FROM Client WHERE ID_client = ?");
            $req->execute([$method['id']]);

            echo json_encode(["success" => true]);
        } else echo json_encode(["success" => false, "error" => "Identifiant de l'user non renseigné"]);
        break;

    default:
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}