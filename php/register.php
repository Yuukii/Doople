<?php
require_once("db_connect.php");

if($_SERVER['REQUEST_METHOD'] == 'GET'){
    echo json_encode(['succes' =>  false, "erreur" => "ce n'est pas bon" ]);
    die;
}

if(isset($_POST['username'], $_POST['pwd'], $_POST['prenom'], $_POST['nom'], $_POST['email'], $_POST['telephone'], $_POST['age'], $_POST['ville'],  $_POST['code_postal'],  $_POST['numero_de_rue'],  $_POST['nom_de_la_rue']) &&
    !empty(trim($_POST['username']))&&
    !empty(trim($_POST['pwd']))&&
    !empty(trim($_POST['prenom']))&&
    !empty(trim($_POST['nom']))&&
    !empty(trim($_POST['email']))&&
    !empty(trim($_POST['telephone']))&&
    !empty(trim($_POST['age']))&&
    !empty(trim($_POST['code_postal']))&&
    !empty(trim($_POST['ville']))&&
    !empty(trim($_POST['numero_de_rue']))&&
    !empty(trim($_POST['nom_de_la_rue'])))
{
    if(!preg_match("/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/", $_POST['pwd'])) {
        echo json_encode(['succes' => false, "erreur" => " pas bon password"]);
        die;
    }

    if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $_POST['email'])) {
        echo json_encode(['succes' => false, "erreur" => " pas bon mail"]);
        die;

    }

    $hash = password_hash($_POST['pwd'], PASSWORD_DEFAULT);
    $sql = "INSERT INTO Client(username, pwd, prenom, nom, email, telephone, age, code_postal, ville, numero_de_rue, nom_de_la_rue) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $req = $db->prepare($sql);
    $req->execute([$_POST['username'], $hash, $_POST['prenom'], $_POST['nom'], $_POST['email'], $_POST['telephone'], $_POST['age'], $_POST['code_postal'], $_POST['ville'], $_POST['numero_de_rue'], $_POST['nom_de_la_rue']]);

    echo json_encode(['success' => true]);
    }else echo json_encode(['success' => false, "erreur" => " incorrect"]);
 ?>
