<?php
session_start();
require_once("db_connect.php");

if($_SERVER['REQUEST_METHOD'] == 'GET'){
    echo json_encode(['success' =>  false, "erreur" => "ce n'est pas bon" ]);
    die;
}

if(isset($_POST['email'], $_POST['pwd']) && !empty(trim($_POST['email'])) && !empty(trim($_POST['pwd']))){
    $sql = "SELECT email, pwd, ID_client, admin FROM Client WHERE email = ?";
    $req = $db->prepare($sql);
    $req->execute([$_POST['email']]);

    $users = $req->fetch(PDO::FETCH_ASSOC);

    if($users && password_verify($_POST['pwd'], $users['pwd'])){
        $_SESSION['connected'] = true;
        $_SESSION['ID_client'] = $users['ID_client'];
        $_SESSION['admin'] = $users['admin'];

        echo json_encode(['success' => true]);
    }  else echo json_encode(['success' => false, "erreur" => "mauvais login"]);

    } else echo json_encode(['success' => false, "erreur" => "champ non rempli"]);
?>